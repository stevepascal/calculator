from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import logging
from functools import partial

class MyButton(QPushButton):

    def __init__(self, text: str, *args, **kwargs):
        super(QPushButton, self).__init__(*args, **kwargs)

        def on_button_clicked(text):
            loggin.warning(text)


        button_size = 60
        self.setFixedHeight(button_size)
        self.setFixedWidth(button_size)

        self.clicked.connect(partial(on_button_clicked, text))



class My_QLineEdit(QLineEdit):
    def __init__(self):
        super(QLineEdit, self).__init__()

        self.setAlignment(Qt.AlignRight)
        # self.setObjectName("My_QLineEdit")
        # self.setStyleSheet('QWidget#My_QLineEdit {background-color: #1d1d1d ;}')


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        window = QWidget()
        base_layout = QGridLayout()
        number_grid = QGridLayout()

        line_edit = My_QLineEdit()

        base_layout.addWidget(line_edit, 0, 0)
        base_layout.addLayout(number_grid, 1, 0)

        number_grid.addWidget(MyButton(self, '1'), 0, 0)
        number_grid.addWidget(MyButton(self, '2'), 0, 1)
        number_grid.addWidget(MyButton(self, '3'), 0, 2)

        number_grid.addWidget(MyButton(self, '4'), 1, 0)
        number_grid.addWidget(MyButton(self, '5'), 1, 1)
        number_grid.addWidget(MyButton(self, '6'), 1, 2)

        number_grid.addWidget(MyButton(self, '7'), 2, 0)
        number_grid.addWidget(MyButton(self, '8'), 2, 1)
        number_grid.addWidget(MyButton(self, '9'), 2, 2)

        window.setLayout(base_layout)
        window.setWindowTitle('Calculator')

        self.resize(350, 450)
        self.setWindowTitle('Calulator')
        self.setCentralWidget(window)
        self.show()

        # logging.info("Window created!")
        # logging.warning('Watch out!')

if __name__ == '__main__':
    app = QApplication([])
    main_window = MainWindow()
    app.exec_()
