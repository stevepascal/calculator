class Logic():
    def do_math(equation):
        # to be returned
        result = 0

        sum_eq = equation.split("+")

        for sum_val in sum_eq:
            sub_eq = sum_val.split('-')
            if len(sub_eq) > 1:
                for idx, val in enumerate(sub_eq):
                    if idx == 0:
                        result = result + int(val)
                    else:
                        result = int(result) - int(val)
            else:
                result = int(result) + int (sum_val)


        return result

if __name__ == '__main__':
    Logic.do_math('5+2+3-4')